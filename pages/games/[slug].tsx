import DetailPage, {getServerSideProps} from 'src/pages/Games/DetailPage/DetailPage';

export {getServerSideProps};

export default DetailPage;
