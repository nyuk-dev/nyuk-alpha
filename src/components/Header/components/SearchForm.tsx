import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSearch} from '@fortawesome/free-solid-svg-icons';

declare interface IProps {
  q?: string;
}

function SearchForm(props: IProps) {
  return (
    <div className="search-container">
      <form method={"GET"} action={"/search"}>
        <div className="search-input">
          <input type="text" className="form-control" placeholder="Từ khóa tìm kiếm" id={"q"} name={"q"} min={6} required={true} defaultValue={props.q}/>
        </div>

        <div className="search-action">
          <FontAwesomeIcon icon={faSearch}/>
        </div>
      </form>
    </div>
  );
}

export default SearchForm;
