import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faCog} from '@fortawesome/free-solid-svg-icons';

import SearchForm from './components/SearchForm';

declare interface IProps {
  q?: string;
}

function Header(props: IProps) {
  return (
    <section className="HeaderComponent">
      <div className="section-1st">
        <a href={"/"}>
          <img src={"/assets/logo.svg"} alt={"Logo"}/>
        </a>
      </div>

      <div className="section-2nd">
        <SearchForm q={props.q}/>
      </div>

      <div className="section-3rd">
        <a href={"/"}>
          <FontAwesomeIcon icon={faCog} fixedWidth={true}/>
        </a>
      </div>
    </section>
  );
}

export default Header;
