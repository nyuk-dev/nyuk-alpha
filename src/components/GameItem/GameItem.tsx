import React from 'react';
import moment from 'moment';

import {GameModel} from 'src/models/GameModel';

declare interface IProps {
  game: GameModel;
}

function GameItem(props: IProps) {
  const {game} = props;

  return (
    <section className="GameItem mt-4">
      <div className="container">
        <div className="card">
          <div className="card-body">
            <div className="section-1st">
              <div className="item-cover" style={{backgroundImage: "url('" + game.coverURL + "')"}}/>

              <h4 className="item-name">
                <a href={`/games/${game.slug}`}>
                  {game.name}
                </a>
              </h4>
            </div>

            <div className="section-2nd">
              <h4 className="item-name">
                <a href={`/games/${game.slug}`}>
                  {game.name}
                </a>
              </h4>

              <p className="item-summary">
                {game.summary}
              </p>

              <p>
                <strong>Genres:</strong> {generateGenres(game)}
              </p>
            </div>

            <div className="section-3rd">
              <p>
                <strong>Platforms:</strong> {generatePlatforms(game)}
              </p>

              <p>
                <strong>Release Date:</strong> {generateReleaseDate(game)}
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default GameItem;

function generateGenres(game: GameModel): string {
  let genres = [];
  game.genres.forEach((genre) => {
    genres.push(genre.name);
  })

  return genres.join(', ');
}

function generatePlatforms(game: GameModel): string {
  let platforms = [];
  game.platforms.forEach((platform) => {
    platforms.push(platform.abbreviation);
  })

  return platforms.join(', ');
}

function generateReleaseDate(game: GameModel): string {
  return moment.utc(game.firstReleaseDate, 'X').format('MMM Do, YYYY');
}
