import Error from 'next/error';
import React from 'react';

import Header from 'src/components/Header/Header';
import GameItem from 'src/components/GameItem/GameItem';
import Game from 'src/models/Game';
import {GameModel} from 'src/models/GameModel';

declare interface IProps {
  statusCode?: number;
  game?: GameModel;
}

function DetailPage(props: IProps) {
  if (props.statusCode != null) {
    return <Error statusCode={props.statusCode}/>;
  }

  return (
    <section id="Games-DetailPage">
      <Header/>

      <GameItem game={props.game}/>
    </section>
  );
}

export async function getServerSideProps({query}) {
  try {
    const game = await Game.getBySlug(query.slug);

    return {
      props: {game}
    };
  } catch (error) {
    let statusCode = 404;
    if (error.response && error.response.status) {
      statusCode = error.response.status;
    }

    return {
      props: {
        statusCode: statusCode,
      }
    };
  }
}

export default DetailPage;
