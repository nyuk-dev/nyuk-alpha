import React from 'react';

import Header from 'src/components/Header/Header';

function HomePage() {
  return (
    <section id="General-HomePage">
      <Header/>
    </section>
  );
}

export default HomePage;
