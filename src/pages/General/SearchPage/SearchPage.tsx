import React from 'react';

import GameItem from 'src/components/GameItem/GameItem';
import Header from 'src/components/Header/Header';
import Game from 'src/models/Game';
import {GameModel} from 'src/models/GameModel';

declare interface IProps {
  games?: GameModel[];
  q?: string;
}

function SearchPage(props: IProps) {
  return (
    <section id="General-SearchPage">
      <Header q={props.q}/>

      {props.games.map((game) => (
        <GameItem key={game.slug} game={game}/>
      ))}
    </section>
  );
}

export default SearchPage;

export async function getServerSideProps({query}) {
  const q = query.q;
  const games = q.length < 4 ? [] : await Game.search(q);

  return {
    props: {
      games: games,
      q: query.q,
    },
  };
}
