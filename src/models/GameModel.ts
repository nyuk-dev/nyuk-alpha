export interface GameModel {
  slug: string;
  name: string;
  summary: string;
  coverURL: string;
  firstReleaseDate: number;
  platforms: GamePlatformModel[];
  genres: GameGenreModel[];
  screenshots: string[];
  trailers: string[];
}

export interface GamePlatformModel {
  slug: string;
  name: string;
  abbreviation: string;
  release_date: number;
}

export interface GameGenreModel {
  slug: string;
  name: string;
}
